﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.MLAgents;
using Unity.MLAgents.Sensors;
public class PenguinAgent : Agent
{
    public float moveSpeed = 5f;

    public float turnSpeed = 180f;

    public GameObject heartPrefab;

    public GameObject regurgitatedFishPrefab;

    private PenguinArea penguinArea;
    new private Rigidbody rigidbody;
    private GameObject baby;
    private bool isFull;
    private float feedRadius = 0f;

    //Start des MLAgents
    public override void Initialize()
    {
        base.Initialize();
        penguinArea = GetComponentInParent<PenguinArea>();
        baby = penguinArea.penguinBaby;
        rigidbody = GetComponent<Rigidbody>();
    }

    //Übersetzung der Entscheidungen des Models auf den Agenten
    public override void OnActionReceived(float[] vectorAction)
    {
        //Konvertierung der Rückgabewerte
        float forwardAmount = vectorAction[0];
        float turnAmount = 0f;
        if (vectorAction[1] == 1f)
        {
            turnAmount = -1f;
        }
        else if (vectorAction[1] == 2f)
        {
            turnAmount = 1f;
        }


        rigidbody.MovePosition(transform.position + transform.forward * forwardAmount * moveSpeed * Time.fixedDeltaTime);
        transform.Rotate(transform.up * turnAmount * turnSpeed * Time.fixedDeltaTime);

        //Negativpunkte nach jedem Schritt um Stillstehen zu vermeiden
        if (MaxStep > 0) AddReward(-1f / MaxStep);
    }

    //Erlaubt es uns den MLAgent manuell zu steuern
    public override void Heuristic(float[] actionsOut)
    {

        actionsOut[0] = 0f;
        actionsOut[1] = 0f;

        if (Input.GetKey(KeyCode.Z))
        {
            //Bewegung nach vorne
            actionsOut[0] = 1f;
        }
        if (Input.GetKey(KeyCode.Q))
        {
            //Linksdrehung
            actionsOut[1] = 1f;
        }
        else if (Input.GetKey(KeyCode.D))
        {
            //Rechtsdrehung
            actionsOut[1] = 2f;
        }

    }
    //wird immer ausgeführt, wenn eine neue Trainingseinheit startet
    public override void OnEpisodeBegin()
    {
        isFull = false;
        penguinArea.ResetArea();
        feedRadius = Academy.Instance.EnvironmentParameters.GetWithDefault("feed_radius", 0f);
    }

    public override void CollectObservations(VectorSensor sensor)
    {
        //Magenzustand; Voll oder Leer (1 float = 1 Wert)
        sensor.AddObservation(isFull);

        //Distanz zum Pinguinbaby (1 float = 1 Wert)
        sensor.AddObservation(Vector3.Distance(baby.transform.position, transform.position));

        //Richtung zum Pinguinbaby (x,y,z = 3 Werte)
        sensor.AddObservation((baby.transform.position - transform.position).normalized);

        //Richtung in der der Agent schaut (x,y,z = 3 Werte)
        sensor.AddObservation(transform.forward);

        //1 + 1 + 3 + 3 = 8 Werte insgesamt 
    }

    private void FixedUpdate()
    {
        //Ist Agent nah genug am Baby zum füttern
        if (Vector3.Distance(transform.position, baby.transform.position) < feedRadius)
        {
            RegurgitateFish();
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.CompareTag("fish"))
        {
            EatFish(collision.gameObject);
        }
        else if (collision.transform.CompareTag("baby"))
        {
            RegurgitateFish();
        }
    }

    private void EatFish(GameObject fishObject)
    {
        if (isFull) return;
        isFull = true;

        penguinArea.RemoveSpecificFish(fishObject);

        AddReward(1f);
    }

    private void RegurgitateFish()
    {
        if (!isFull) return; 
        isFull = false;

        GameObject regurgitatedFish = Instantiate<GameObject>(regurgitatedFishPrefab);
        regurgitatedFish.transform.parent = transform.parent;
        regurgitatedFish.transform.position = baby.transform.position;
        Destroy(regurgitatedFish, 4f);

        GameObject heart = Instantiate<GameObject>(heartPrefab);
        heart.transform.parent = transform.parent;
        heart.transform.position = baby.transform.position + Vector3.up;
        Destroy(heart, 4f);

        AddReward(1f);

        if (penguinArea.FishRemaining <= 0)
        {
            EndEpisode();
        }
    }
}

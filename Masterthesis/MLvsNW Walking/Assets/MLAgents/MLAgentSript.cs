﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.MLAgents;
using Unity.MLAgents.Sensors;
using System;

public class MLAgentSript : Agent
{
    [SerializeField]
    private float speed = 10.0f;
    [SerializeField]
    private Transform target;
    [SerializeField]
    private bool hitObstacle = false;
    [SerializeField]
    private float distanceRequired = 0.5f;
    [SerializeField]
    private Material successMaterial;
    [SerializeField]
    private Material defaultMaterial;
    [SerializeField]
    private Material failMaterial;
    [SerializeField]
    private MeshRenderer groundMeshRenderer;

    private Rigidbody mlAgentRgBody;
    private Vector3 originalPosition;
    //Start des MLAgents
    public override void Initialize()
    {
        mlAgentRgBody = GetComponent<Rigidbody>();
        originalPosition = transform.localPosition;
    }

    //wird immer ausgeführt, wenn eine neue Trainingseinheit startet
    public override void OnEpisodeBegin()
    {
        hitObstacle = false;
        transform.localPosition = originalPosition;
        transform.localPosition = new Vector3(originalPosition.x, originalPosition.y, UnityEngine.Random.Range(originalPosition.z-0.75f,originalPosition.z + 0.75f));
    }
    //Gebe dem Tensorflow framework die zu observierenden Daten
    public override void CollectObservations(VectorSensor sensor)
    {
        // 3 observations (x,y,z)
        sensor.AddObservation(transform.localPosition);
        // 3 observations (x,y,z)
        sensor.AddObservation(target.transform.localPosition);
        // 1 observations (x)
        sensor.AddObservation(mlAgentRgBody.velocity.x);
        // 1 observations (z)
        sensor.AddObservation(mlAgentRgBody.velocity.z);
    }
    //Fangen die Aktionen vom Tensorflow Framework ab und fügen sie dem MLAgent hinzu
    public override void OnActionReceived(float[] vectorAction)
    {
        Vector3 vectorForce = new Vector3();
        vectorForce.x = vectorAction[0];
        vectorForce.z = vectorAction[1];


        mlAgentRgBody.AddForce(vectorForce * speed);
        float distanceFromTarget = Vector3.Distance(transform.localPosition, target.transform.localPosition);

        float newDistance = Vector3.Distance(transform.position, target.position);
        
        if (distanceFromTarget <= distanceRequired)
        {
            AddReward(10.0f);
            EndEpisode();
            StartCoroutine(SwapGroundMaterial(successMaterial, 0.5f));
        }
        //Bei Kollision wird die Trainingseinheit wiederholt
        if (hitObstacle)
        {
            EndEpisode();
            AddReward(-0.1f);
            StartCoroutine(SwapGroundMaterial(failMaterial, 0.5f));
            hitObstacle = false;
        }

    }
    //Erlaubt es uns den MLAgent manuell zu steuern
    public override void Heuristic(float[] actionsOut)
    {
        actionsOut[0] = Input.GetAxis("Horizontal"); // X-Wert
        actionsOut[1] = Input.GetAxis("Vertical"); // Z-Wert
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.gameObject.tag == "Obstacle" && !hitObstacle)
        {
            hitObstacle = true;
        }
    }

    private IEnumerator SwapGroundMaterial(Material mat, float time)
    {
        groundMeshRenderer.material = mat;
        yield return new WaitForSeconds(time);
        groundMeshRenderer.material = defaultMaterial;
    }
}

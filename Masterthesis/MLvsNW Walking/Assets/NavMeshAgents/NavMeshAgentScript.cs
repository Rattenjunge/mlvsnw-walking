﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class NavMeshAgentScript : MonoBehaviour
{
    [SerializeField]
    private Transform target;
    [SerializeField]
    private Material successMaterial;
    [SerializeField]
    private Material defaultMaterial;
    [SerializeField]
    private Material failMaterial;
    [SerializeField]
    private MeshRenderer groundMeshRenderer;

    private Vector3 startPosition;
    
    private void Start()
    {
        startPosition = transform.localPosition;
        NavMeshAgent agent = GetComponent<NavMeshAgent>();
        agent.destination = target.position;
        transform.localPosition = new Vector3(startPosition.x, startPosition.y, Random.Range(startPosition.z - 0.75f, startPosition.z + 0.75f));
    }
    private void Update()
    {
        
        if (Vector3.Distance(transform.localPosition, target.localPosition) < 2f)
        {
            transform.localPosition = new Vector3(startPosition.x, startPosition.y, Random.Range(startPosition.z - 0.75f, startPosition.z + 0.75f));
            StartCoroutine(SwapGroundMaterial(successMaterial, 0.5f));
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.collider.tag == "Obstacle")
        {
            transform.localPosition = new Vector3(startPosition.x, startPosition.y, Random.Range(startPosition.z - 0.75f, startPosition.z + 0.75f));
            StartCoroutine(SwapGroundMaterial(failMaterial, 0.5f));
        }
    }

    private IEnumerator SwapGroundMaterial(Material mat, float time)
    {
        groundMeshRenderer.material = mat;
        yield return new WaitForSeconds(time);
        groundMeshRenderer.material = defaultMaterial;
    }
}

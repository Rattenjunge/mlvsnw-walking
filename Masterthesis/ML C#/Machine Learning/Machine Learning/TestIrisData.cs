﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Machine_Learning
{
    class TestIrisData
    {
        //Testdaten für das Model
        internal static readonly IrisData Setosa =
            new IrisData
            {
                SepalLength = 5.4f,
                SepalWidth = 3.2f,
                PetalLength = 1.3f,
                PetalWidth = 0.5f
            };
    }
}

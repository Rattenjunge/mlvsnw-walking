﻿using Microsoft.ML;
using System;
using System.IO;

namespace Machine_Learning
{
    class Program
    {
        static readonly string _dataPath = Path.Combine(Environment.CurrentDirectory, "Data", "iris.data");
        static readonly string _modelPath = Path.Combine(Environment.CurrentDirectory, "Data", "IrisClusteringModel.zip");
        static void Main(string[] args)
        {
            //Context = Umbegung in der das Model trainiert wird
            var mlContext = new MLContext(seed: 0);
            IDataView dataView = mlContext.Data.LoadFromTextFile<IrisData>(_dataPath, hasHeader: false, separatorChar: ',');



            //Die Pipeline bestimmt, welche Daten durch das Neuronale Netzwerk laufen und welcher Algorithmus verwendet wird. 
            //(Beim KMeans Algorithmus, wird auch bestimmt, wieviele Cluster gewollt sind)
            string featuresColumnName = "Features";
            var pipeline = mlContext.Transforms.Concatenate(featuresColumnName,
                "SepalLength",
                "SepalWidth",
                "PetalLength",
                "PetalWidth").Append(mlContext.Clustering.Trainers.KMeans(featuresColumnName, numberOfClusters: 3));

            var model = pipeline.Fit(dataView);

            //Model wird als File gespeichert
            using (var fileStream = new FileStream(_modelPath,
                FileMode.Create,
                FileAccess.Write,
                FileShare.Write))
            {
                mlContext.Model.Save(model, dataView.Schema, fileStream);
            }
            //Erstellt eine PredictionEngine die mithilfe des Models den Input zu einem Cluster zuordnet
            var predictor = mlContext.Model.CreatePredictionEngine<IrisData, ClusterPrediction>(model);

            var prediction = predictor.Predict(TestIrisData.Setosa);

            Console.WriteLine($"Cluster: {prediction.PredictedClusterID}");
            Console.WriteLine($"Distance: {string.Join(" ", prediction.Distance)}");
            Console.ReadLine();

        }
    }
}

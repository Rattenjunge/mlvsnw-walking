﻿using Microsoft.ML.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace Machine_Learning
{
    //Beschreibt wie der Output auszusehen hat
    class ClusterPrediction
    {
        [ColumnName("PredictedLabel")]
        public uint PredictedClusterID;

        [ColumnName("Score")]
        public float[] Distance;
    }
}

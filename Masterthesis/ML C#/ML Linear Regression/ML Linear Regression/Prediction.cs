﻿using Microsoft.ML.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace ML_Linear_Regression
{
    public class Prediction
    {
        [ColumnName("Score")]
        public float Price { get; set; }
    }
}

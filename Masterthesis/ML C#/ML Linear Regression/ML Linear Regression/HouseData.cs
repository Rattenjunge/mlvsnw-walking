﻿using Microsoft.ML.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace ML_Linear_Regression
{
    public class HouseData
    {
        [LoadColumn(0)]
        public float Size { get; set; }
        [LoadColumn(1)]
        public float Price { get; set; }
    }
}

﻿using Microsoft.ML;
using System;
using System.IO;

namespace ML_Linear_Regression
{
    class Program
    {
        static readonly string _dataPath = Path.Combine(Environment.CurrentDirectory, "Data", "house.data");
        static readonly string _modelPath = Path.Combine(Environment.CurrentDirectory, "Data", "HouseModel.zip");
        static void Main(string[] args)
        {
            //Context = Umbegung in der das Model trainiert wird
            MLContext mlContext = new MLContext();
            IDataView dataView = mlContext.Data.LoadFromTextFile<HouseData>(_dataPath, hasHeader: false, separatorChar: ',');

            //Die Pipeline bestimmt, welche Daten durch das Neuronale Netzwerk laufen und welcher Algorithmus verwendet wird. 
            string featuresColumnName = "Features";
            var pipeline = mlContext.Transforms.Concatenate(featuresColumnName, new[] { "Size" })
                .Append(mlContext.Regression.Trainers.Sdca(labelColumnName: "Price", maximumNumberOfIterations: 100));

            var model = pipeline.Fit(dataView);

            //Model wird als File gespeichert
            using (var fileStream = new FileStream(_modelPath,
                FileMode.Create,
                FileAccess.Write,
                FileShare.Write))
            {
                mlContext.Model.Save(model, dataView.Schema, fileStream);
            }

            //Erstellung eines Testhauses
            var size = new HouseData() { Size = 2.5f };
            //Vorhersage des Preises des Testhauses
            var price = mlContext.Model.CreatePredictionEngine<HouseData, Prediction>(model).Predict(size);

            Console.WriteLine($"Predicted price for size: {size.Size * 1000} sq ft = {price.Price * 100:C}k");
            Console.ReadLine();
        }
    }
}
